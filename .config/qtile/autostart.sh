#!/usr/bin/env bash
systemctl start cronie &
mako &
sh ~/.config/qtile/Script/battery-alert.sh &
picom -b --experimental-backends &
eww daemon &
