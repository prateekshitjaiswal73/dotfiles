#  ________  _________  ___  ___       _______      
# |\   __  \|\___   ___\\  \|\  \     |\  ___ \     
# \ \  \|\  \|___ \  \_\ \  \ \  \    \ \   __/|    
#  \ \  \\\  \   \ \  \ \ \  \ \  \    \ \  \_|/__  
#   \ \  \\\  \   \ \  \ \ \  \ \  \____\ \  \_|\ \ 
#    \ \_____  \   \ \__\ \ \__\ \_______\ \_______\
#     \|___| \__\   \|__|  \|__|\|_______|\|_______|
#           \|__|                                   


# -*- coding: utf-8 -*-


import os
import subprocess
from libqtile import bar, layout, widget, extension, hook, qtile
from libqtile.config import Click, Drag, Group, Key, Match, Screen, KeyChord
from libqtile.lazy import lazy
from libqtile import hook
from qtile_extras import widget
from qtile_extras.widget.decorations import BorderDecoration
import psutil

mod = "mod4"
terminal = "alacritty"
keys = [
    ### The Essentials
    Key([mod], "Return",
        lazy.spawn(terminal),
        desc="Launch terminal"
        ),
    Key([mod, "shift"], "Return",
        lazy.spawn("rofi -show drun"), # dmenu-wl_run -b -i -sb #51afef -sf #282c34 -p 'Run: '"),
        desc="Spawn a command using a prompt widget"
        ),
    Key([mod], "b",
        lazy.spawn("librewolf"),
        desc="Launches Web Browser"
        ),
    Key([mod], "t",
        lazy.spawn("thunar"),
        desc="Launches File Manager"
        ),
    Key([mod, "shift"], "v",
        lazy.spawn("alacritty -e nvim"),
        desc="Launches NeoVim Text Editor"
        ),
    Key([], "Print",
        lazy.spawn("sh /home/coding/.config/qtile/Scripts/screenshot.sh"),
        desc="Takes a screenshot and opens it in Shutter"
        ),

    # Toggle between different layouts as defined below
    Key([mod], "Tab",
        lazy.next_layout(),
        desc="Toggle between layouts"),
    Key([mod], "m",
        lazy.window.toggle_fullscreen(),
        desc="Toggle fullscreen on the focused window",
    ),
    Key([mod], "f",
        lazy.window.toggle_floating(),
        desc="Toggle floating on the focused window"
        ),
    Key([mod], "space",
        lazy.layout.next(),
        desc="Move window focus to other window"
        ),

    # Switch between windows
    Key([mod], "h",
        lazy.layout.left(),
        desc="Move focus to left"
        ),
    Key([mod], "l",
        lazy.layout.right(),
        desc="Move focus to right"
        ),
    Key([mod], "j",
        lazy.layout.down(),
        desc="Move focus down"
        ),
    Key([mod], "k",
        lazy.layout.up(),
        desc="Move focus up"
        ),

    # Move windows between left/right columns or move up/down in current stack.
    # Moving out of range in Columns layout will create new column.
    Key([mod, "shift"], "h",
        lazy.layout.shuffle_left(),
        desc="Move window to the left"
        ),
    Key([mod, "shift"], "l",
        lazy.layout.shuffle_right(),
        desc="Move window to the right"
        ),
    Key([mod, "shift"], "j",
        lazy.layout.shuffle_down(),
        desc="Move window down"
        ),
    Key([mod, "shift"], "k",
        lazy.layout.shuffle_up(),
        desc="Move window up"
        ),

    # Grow windows. If current window is on the edge of screen and direction
    # will be to screen edge - window would shrink.
    Key([mod, "control"], "h",
        lazy.layout.grow_left(),
        desc="Grow window to the left"
        ),
    Key([mod, "control"], "l",
        lazy.layout.grow_right(),
        desc="Grow window to the right"
        ),
    Key([mod, "control"], "j",
        lazy.layout.grow_down(),
        desc="Grow window down"
        ),
    Key([mod, "control"], "k",
        lazy.layout.grow_up(),
        desc="Grow window up"
        ),
    Key([mod], "n",
        lazy.layout.normalize(),
        desc="Reset all window sizes"
        ),

    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    # Key([mod, "shift"], "s",
    #    lazy.layout.toggle_split(),
    #    desc="Toggle between split and unsplit sides of stack",
    #),

    Key([mod, "shift"], "c",
        lazy.window.kill(),
        desc="Kill focused window"
        ),
    Key([mod, "control"], "r",
        lazy.reload_config(),
        desc="Reload the config"
        ),

    #Key([mod, "control"], "q",
    #    lazy.shutdown(),
    #    desc="Shutdown Qtile"
    #    ),
    #Key([mod, "shift"], "r",
    #    lazy.spawn('./.dmenu/dmprompt "Are you sure , you want to reboot ? " "systemctl reboot"'),
    #    desc='Reboot'
    #    ),
    #Key([mod, "shift"], "q",
    #    lazy.spawn('./.dmenu/dmprompt "Are you sure , you want to shutdown ? " "systemctl poweroff"'),
    #    desc='Shutdown'
    #    ),
    #Key([mod, "shift"], "l",
    #    lazy.shutdown(),
    #    desc='Logout'
    #    ),

    Key([mod, "shift"], "l",
        lazy.spawn("./.rofi/rofi-logout"),
        desc="Logout Menu"
        ),

    KeyChord([mod], "r", [
        Key([], "e",
            lazy.spawn("./.rofi/rofi-conf"), # "./.dmenu/dmconf"),
            desc='Choose a config file to edit'
            ),
        Key([], "a",
            lazy.spawn("./.rofi/rofi-sounds"), # "./.dmenu/dmsounds"),
            desc='Ambiant Sounds'
            ),
        Key([], "s",
            lazy.spawn("./.rofi/rofi-search"), # "./.dmenu/dmsearch"),
            desc='Search via rofi'
            ),
        Key([], "h",
            lazy.spawn("./.rofi/rofi-man"), # "./.dmenu/dman"),
            desc='Man page viewer'
            ),
        Key([], "w",
            lazy.spawn("./.rofi/rofi-wifi"), # "./.dmenu/dmwifi"),
            desc='Monitor WiFi'
            ),
    ]),
]

groups = [Group(i) for i in "123456789"]
#groups = [Group(f"{i+1}", label="") for i in range(8)]

for i in groups:
    keys.extend(
        [
            # mod1 + group number = switch to group
            Key(
                [mod],
                i.name,
                lazy.group[i.name].toscreen(),
                desc="Switch to group {}".format(i.name),
            ),
            # mod1 + shift + group number = switch to & move focused window to group
            Key(
                [mod, "shift"],
                i.name,
                lazy.window.togroup(i.name, switch_group=True),
                desc="Switch to & move focused window to group {}".format(i.name),
            ),
            # Or, use below if you prefer not to switch to that group.
            # # mod1 + shift + group number = move focused window to group
            # Key([mod, "shift"], i.name, lazy.window.togroup(i.name),
            #     desc="move focused window to group {}".format(i.name)),
        ]
    )

def init_layout_theme():
    return {
            "margin":20,
            "border_width":3,
            "border_focus": "e1acff", # "#5e81ac",
            "border_normal": "1D2330" # "#4c566a"
            }

layout_theme = init_layout_theme()

layouts = [
    # layout.Columns(border_focus_stack=["#d75f5f", "#8f3d3d"], border_width=4),
    layout.MonadTall(**layout_theme),
    layout.Max(**layout_theme),
    layout.Floating(),
    # Try more layouts by unleashing below layouts.
    # layout.Stack(num_stacks=2),
    # layout.Bsp(),
    # layout.Matrix(),
    # layout.MonadWide(),
    # layout.RatioTile(),
    # layout.Tile(),
    # layout.TreeTab(),
    # layout.VerticalTile(),
    # layout.Zoomy(),
]

widget_defaults = dict(
    font="SourceCodeVF Semibold",
    fontsize=12,
    padding=2,
)
extension_defaults = widget_defaults.copy()

screens = [
    Screen(
        wallpaper = '~/Wallpapers/000.jpg', #2, 6, 8, 11, 13, 16, 17
        wallpaper_mode = 'fill',
        top=bar.Bar(
            [
                widget.Spacer(length=5,
                    background='#282738',
                    ),
                widget.Image(
                    filename='~/.config/qtile/Assets/launch_Icon.png',
                    margin=2,
                    background='#282738',
                ),
                widget.Image(
                    filename='~/.config/qtile/Assets/6.png',
                ),
                widget.GroupBox(
                    fontsize=24,
                    borderwidth=3,
                    highlight_method='block',
                    active='#CAA9E0',
                    block_highlight_text_color="#91B1F0",
                    highlight_color='#4B427E',
                    inactive='#282738',
                    foreground='#4B427E',
                    background='#353446',
                    this_current_screen_border='#353446',
                    this_screen_border='#353446',
                    other_current_screen_border='#353446',
                    other_screen_border='#353446',
                    urgent_border='#353446',
                    rounded=True,
                    disable_drag=True,
                    ),
                widget.Prompt(),
                widget.Spacer(
                    length=8,
                    background='#353446',
                ),
                widget.Image(
                    filename='~/.config/qtile/Assets/1.png',
                ),
                widget.CurrentLayout(
                    background='#353446',
                    foreground='#CAA9E0',
                    fmt='{}',
                    font="JetBrains Mono Bold",
                    fontsize=15,
                    ),
                widget.Spacer(
                    length=8,
                    background='#353446',
                ),
                widget.CurrentLayoutIcon(
                    custom_icon_path=[os.path.expanduser('~/.config/qtile/Assets/icons')],
                    background="#353446",
                    scale=0.6,
                ),
                widget.Image(
                    filename='~/.config/qtile/Assets/5.png',
                ),
                widget.WindowName(
                    background = '#353446',
                    format = "{name}",
                    font='JetBrains Mono Bold',
                    foreground='#CAA9E0',
                    empty_group_string = 'Desktop',
                    fontsize=15,
                    ),
                # NB Systray is incompatible with Wayland, consider using StatusNotifier instead
                # widget.StatusNotifier(icon_theme="Obsidian-Mint"),
                widget.Image(
                    filename='~/.config/qtile/Assets/3.png',
                ),
                widget.Wlan(
                    background='#282738',
                    foreground='#CAA9E0',
                    fontsize=15,
                    format='  {essid} {percent:2.0%}  ',
                    interface='wlp2s0',
                    disconnected_message='No Wifi',
                    font='JetBrains Mono Bold',
                    mouse_callbacks={'Button1': lambda: qtile.cmd_spawn('sh /home/coding/.config/qtile/Scripts/systray-Wifi.sh')},
                ),
                widget.Image(
                    filename="~/.config/qtile/Assets/6.png",
                    background="#353446",
                ),
                widget.TextBox(
                    text=' ',
                    background='#353446',
                ),
                widget.Net(
                    format='{down:.0f}{down_suffix} ↓↑ {up:.0f}{up_suffix}',
                    background='#353446',
                    foreground='#CAA9E0',
                    font="JetBrains Mono Bold",
                    prefix='k',
                    fontsize=15,
                    ),
                widget.Image(
                    filename='~/.config/qtile/Assets/2.png',
                ),
                widget.Spacer(
                    length=3,
                    background='#353446',
                ),
                widget.Image(
                    filename='~/.config/qtile/Assets/Misc/ram.png',
                ),
                widget.CPU(
                    background='#353446',
                    format='{freq_current}GHz {load_percent}%',
                    foreground='#CAA9E0',
                    font="JetBrains Mono Bold",
                    fontsize=15,
                    update_interval=5,
                ),
                widget.Image(
                    filename='~/.config/qtile/Assets/2.png',
                ),
                widget.Spacer(
                    length=8,
                    background='#353446',
                ),
                widget.Image(
                filename='~/.config/qtile/Assets/Misc/ram.png',
                ),
                widget.Memory(
                    background='#353446',
                    format='{MemUsed: .0f}{mm}',
                    foreground='#CAA9E0',
                    font="JetBrains Mono Bold",
                    fontsize=15,
                    update_interval=5,
                ),
                widget.Image(
                    filename='~/.config/qtile/Assets/2.png',
                ),
                widget.Spacer(
                    length=8,
                    background='#353446',
                ),
                widget.BatteryIcon(
                    theme_path='~/.config/qtile/Assets/Battery/',
                    background='#353446',
                    scale=1,
                ),
                widget.Battery(
                    font='JetBrains Mono Bold',
                    background='#353446',
                    foreground='#CAA9E0',
                    format='{percent:2.0%}',
                    fontsize=15,
                ),
                # widget.Image(
                #    filename='~/.config/qtile/Assets/2.png',
                # ),
                # widget.Spacer(
                #    length=3,
                #    background='#353446',
                # ),
                # widget.Volume(
                #    font='JetBrainsMono Nerd Font',
                #    theme_path='~/.config/qtile/Assets/Volume/',
                #    emoji=True,
                #    fontsize=13,
                #    background='#353446',
                # ),
                # widget.Spacer(
                #    length=-5,
                #    background='#353446',
                #    ),
                widget.Image(
                    filename='~/.config/qtile/Assets/5.png',
                    background='#353446',
                ),
                widget.Image(
                    filename='~/.config/qtile/Assets/Misc/clock.png',
                    background='#282738',
                    margin_y=6,
                    margin_x=5,
                ),
                widget.Clock(
                    format='%I:%M %p',
                    background='#282738',
                    foreground='#CAA9E0',
                    font="JetBrains Mono Bold",
                    fontsize=15,
                    mouse_callbacks={'Button1': lambda: qtile.cmd_spawn('eww open calendar && sleep 10s && eww close-all')},
                ),
                widget.Spacer(
                    length=10,
                    background='#282738',
                    ),
            ],
            30,
            opacity = 0.8,
            border_color = '#282738',
            border_width = [0,0,0,0],
            margin = [10,30,0,30],
            background = '#353446',
            #border_width=[2, 0, 2, 0],  # Draw top and bottom borders
            #border_color=["ff00ff", "000000", "ff00ff", "000000"]  # Borders are magenta
        ),
        # You can uncomment this variable if you see that on X11 floating resize/moving is laggy
        # By default we handle these events delayed to already improve performance, however your system might still be struggling
        # This variable is set to None (no cap) by default, but you can set it to 60 to indicate that you limit it to 60 events per second
        # x11_drag_polling_rate = 60,
    ),
]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(), start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(), start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front()),
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: list
follow_mouse_focus = True
bring_front_click = False
floats_kept_above = True
cursor_warp = False
floating_layout = layout.Floating(
    float_rules=[
        # Run the utility of `xprop` to see the wm class and name of an X client.
        *layout.Floating.default_float_rules,
        Match(wm_class="confirmreset"),         # gtk
        Match(wm_class="makebranch"),           # gtk
        Match(wm_class="maketag"),              # gtk
        Match(wm_class="ssh-askpass"),          # ssh-askpass
        Match(title="branchdialog"),            # gtk
        Match(title="pinentry"),                # GPG key password entry
        Match(title='Confirmation'),            # tastyworks exit box
        Match(title='mate-calculator'),         # Calculator
        Match(wm_class='kdenlive'),             # kdenlive
        Match(wm_class='conky'),                # conky
        Match(wm_class='clamtk'),               # Anti-virus
        Match(wm_class='Stacer'),               # Stacer
        Match(wm_class='kitty'),                # Kitty terminal

    ]
)
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = True

# When using the Wayland backend, this can be used to configure input devices.
wl_input_rules = None

@hook.subscribe.startup_once
def start_once():
    subprocess.run('/home/coding/.config/qtile/autostart.sh')

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"

def main(qtile):
	detect_screens(qtile)

