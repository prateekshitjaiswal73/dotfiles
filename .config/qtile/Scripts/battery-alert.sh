#!/usr/bin/env bash

# Check if battery is present
if acpi -b &> /dev/null; then
    s=$(acpi | awk '{print $3}' | sed -e 's/,//g')
    b=$(acpi | awk '{print $4}' | sed -e 's/%,//g')

    if [ "$b" -lt 31 ]; then
        if [ "$s" == "Discharging" ]; then
            notify-send -u critical "Battery running out!!!" "Battery is discharging, please connect the charger mate. ;) "
        fi
    fi
fi
