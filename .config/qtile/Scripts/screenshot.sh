#!/bin/bash

# Takes a screenshot and opens it in Shutter

SCREENSHOT_DIR="$HOME/Pictures/Screenshots"

mkdir -p "$SCREENSHOT_DIR"
cd "$SCREENSHOT_DIR"

SCREENSHOT_NAME="$(date +%Y-%m-%d_%H-%M-%S-%N.png)"
grim $SCREENSHOT_NAME
notify-send "Screenshot" "Screenshot Taken !"
