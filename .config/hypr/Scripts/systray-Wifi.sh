#!/usr/bin/env bash


mkdir /tmp/wifi/
nmcli -t dev wifi > /tmp/wifi/Scan
awk -F ":" '{print $8"\t"$13}' /tmp/wifi/Scan > /tmp/wifi/Name
# shellcheck disable=SC2002
choice=$(cat /tmp/wifi/Name | rofi -dmenu -p "Available Wifi : " -theme-str ' window { background-color: #038eff; }') # dmenu-wl -i -b -sb '#51afef' -sf '#282c34' -p "Available: ")
ssid=$(awk -F "\t" '{print $1}' <<< "$choice")
bssid=$(grep "$ssid" /tmp/wifi/Scan | awk -F ":" '{print $1 $2 $3 $4 $5 $6 $7}' | sed -e 's/\\/:/g' | sed -e 's/\ //g')
c=$(grep -c "$ssid" /tmp/wifi/Scan)
if [ "$c" == "1" ]; then
    if ! nmcli d wifi connect "$bssid"; then
        password=$(rofi -dmenu -p "Password : " -theme-str ' window { background-color: #038eff; }')
        nmcli d wifi connect "$bssid" password "$password"
    fi

fi
rm -rf /tmp/wifi/
