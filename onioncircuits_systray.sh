#!/bin/bash

onioncircuits &

while [ true ]
do
   sleep 7
   status=$(wmctrl -l | grep "onioncircuits")
   if [ "$status" != "" ] ; then
      break
   fi
done

kdocker -m onioncircuits
