#!/bin/bash

cd qtile
git pull
python3 -m pip install -r requirements-dev.txt
python3 -m pip install -r requirements.txt
make clean install
cd docs
make clean install
python3 -m pip install -r requirements.txt
cd ..
python3 -m pip install xcffib
python3 -m pip install --no-cache-dir cairocffi
echo
echo Issue Command : sudo apt-get install libpangocairo-1.0-0
echo
python3 -m pip install dbus-next
python3 -m pip install .
python3 -m pip install qtile
echo
python3 -c 'input("Finished......")'
