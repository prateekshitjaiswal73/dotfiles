#!usr/bin/env bash
#
# Script name: dmbright
# Description: Adjust the brightness of laptop
# Dpendencies: dmenu

# Set with the flags "-e", "-u","-o pipefail" cause the script to fail 
# if certain things happen, which is a good thing.  Otherwise, we can 
# get hidden bugs that are hard to discover. 

set -euo pipefail


# An array of options to choose. 

declare -A options=(
"100 %"
"90%"
"80%"
"70%"
"60%"
"50%"
"40%"
"30%"
"20%"
"10%"
"0%"
)


# Piping the above array into dmenu. 
# We use "printf '%s\n'" to format the array one item to a line. 

choice=$(printf '%s\n' "${options[@]}" | dmenu -i -l 3 -sb '#51afef' -sf '#282c34' -i -p 'Brightness Level : ' "$@") 


# What to do when/if we choose one of the options.

case "$choice" in
    '100%')
        AD=$(xrandr -q | grep ' connected' | head -n 1 | cut -d ' ' -f1)\
            xrandr --output "$AD" --brightness 1
                    ;;
        '90%')
            AD=$(xrandr -q | grep ' connected' | head -n 1 | cut -d ' ' -f1)\
                xrandr --output "$AD" --brightness 0.9 
                            ;;
        '80%')
            AD=$(xrandr -q | grep ' connected' | head -n 1 | cut -d ' ' -f1)\
                xrandr --output "$AD" --brightness 0.8
                            ;;
        '70%')
            AD=$(xrandr -q | grep ' connected' | head -n 1 | cut -d ' ' -f1)\
                xrandr --output "$AD" --brightness 0.7
                            ;;
        '60%')
            AD=$(xrandr -q | grep ' connected' | head -n 1 | cut -d ' ' -f1)\
                xrandr --output "$AD" --brightness 0.6
                            ;;
        '50%')
            AD=$(xrandr -q | grep ' connected' | head -n 1 | cut -d ' ' -f1)\
                xrandr --output "$AD" --brightness 0.5
                            ;;
        '40%')
            AD=$(xrandr -q | grep ' connected' | head -n 1 | cut -d ' ' -f1)\
                xrandr --output "$AD" --brightness 0.4
                            ;;
        '30%')
            AD=$(xrandr -q | grep ' connected' | head -n 1 | cut -d ' ' -f1)\
                xrandr --output "$AD" --brightness 0.3
                            ;;
        '20%')
            AD=$(xrandr -q | grep ' connected' | head -n 1 | cut -d ' ' -f1)\
                xrandr --output "$AD" --brightness 0.2
                            ;;
        '10%')
            AD=$(xrandr -q | grep ' connected' | head -n 1 | cut -d ' ' -f1)\
                xrandr --output "$AD" --brightness 0.1
                            ;;
        '0%')
            AD=$(xrandr -q | grep ' connected' | head -n 1 | cut -d ' ' -f1)\
                xrandr --output "$AD" --brightness 0
                            ;;
        'Quit')
            echo "Program terminated." && exit 0
            ;;
        *)
            exit 0
            ;;
    esac


