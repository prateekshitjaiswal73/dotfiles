#!/bin/bash
#
# Dmenu script for launching system monitoring programs.


declare -a options=("htop
iftop
iotop
iptraf-ng
nmon
s-tui
quit")

choice=$(echo -e "${options[@]}" | dmenu -l 3  -sb '#51afef' -sf '#282c34' -i -p 'System monitors: ')

case $choice in
	quit)
		echo "Program terminated." && exit 1
	;;
	htop| \
	nmon| \
	s-tui)
        exec alacritty -e $choice
	;;
	iftop| \
	iotop| \
	iptraf-ng)
        exec alacritty -e sudo $choice
	;;
	*)
		exit 1
	;;
esac

