#!/usr/bin/env bash

cat <<EOF | xmenu -i -p 0x25:1 | sh &

 Applications
	 Browser
		 Brave	brave-browser
		 Firefox	firefox
		 Qute	qutebrowser
	 Terminal	alacritty
	 Files	thunar
	
	 Comms
		 Email (GUI)		thunderbird
		 WhatsApp (GUI)		alacritty -e snap run whatsdesk
		 Session (GUI)		alacritty -e snap run session-desktop
		 Telegram (GUI)		telegram-desktop
	 Utilities
		 Calculator (GUI)		mate-calculator
		
		 System
			 Docker (Plank)		alacritty -e plank &
			 Kill Window	xkill
			 Screen Recording (OBS)		obs
			 Screenshot		xfce4-screenshooter
		 Theming
			lxappearance 	lxappearance
			Qt (qt5ct) 	qt5ct --style gtk2
		 Monitors
			System (mytop)		alacritty -e mytop
			System (bashtop)		alacritty -e bashtop
			
			Disk Usage (TUI)		alacritty -e ncdu
			IO (iotop)		alacritty -e iotop
			
			Kernel (kmod)		alacritty -e kmod
			
			Bandwidth (bmon)		alacritty -e bmon
	 Entertainment
		 Media
			VLC		vlc
			Parole		parole
			MPV		mpv
		 Games
			 Steam	steam
			ﳛ Xonotic		xonotic-glx
		 Misc
			KDenlive		sh -c (cd $HOME/Application/kdenlive.appimage/ ; npm start)
			Stacer		sh -c (cd $HOME/Application/Stacer.AppImage/ ; npm start)
			LBRY		sh -c (cd $HOME/Application/LBRY.AppImage/ ; npm start)
			cmatrix		alacritty -e cmatrix -B
			eDEX-UI		sh -c (cd $HOME/Application/eDEX-UI.AppImage/ ; npm start)
	 Science
		 Astronomy
			Celestia	celestia
		 Chemistry
			Periodic Table	alacritty -e gperiodic
	 Development
		 IDEs
			 Neovim	alacritty -e nvim
			 IDLE		idle
			 Mousepad	mousepad
		QDbusViewer	qdbusviewer --style gtk2	

 Configs
	 System
		 Qtile	alacritty -e nvim $CONFIGDIR/qtile/config.py
		 Start Menu	alacritty -e nvim $HOME/xmenu.sh
		 Notifications	alacritty -e nvim $CONFIGDIR/.dunst/dunstrc
		 Sound	pavucontrol
		 Shell
			bash	alacritty -e nvim $HOME/.bashrc
			zsh	alacritty -e nvim $HOME/.zshrc
		 rofi
			config	alacritty -e nvim $CONFIGDIR/rofi/config
			template	alacritty -e nvim $CONFIGDIR/wal/templates/custom-rofi.rasi
		 .Xresources	alacritty -e nvim $HOME/.Xresources
	 User
		neovim		alacritty -e nvim $HOME/.config/nvim/init.vim
		neofetch	alacritty -e nvim $CONFIGDIR/neofetch/config.conf
		htop	alacritty -e nvim $CONFIGDIR/htop/htoprc
		s-tui	alacritty -e nvim $CONFIGDIR/s-tui/s-tui.conf

 System
	 Shutdown		poweroff
	 Reboot			reboot
EOF
