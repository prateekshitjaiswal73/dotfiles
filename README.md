<img src="My_DotFiles.png" />

# My Dot Files ![PowerShell Gallery](https://img.shields.io/powershellgallery/p/Az?color=gree&logo=linux&style=plastic)


:star: Star us on GitLab — it motivates us a lot!

> Some of my dot and conifg files.

**NOTE :** These were made and setuped according to my computer. Will try to provide the solutions for common problem. Do not forget to open an issue if things break for you with a genuine reason.

> To execute those solutions just do :-
    
```
chmod +x [Name]
./[Name]
```
> where [Name] = Name of the file
>> Mostly it will start with " Solution for ... " for ease of work.

It's not all my work :sweat_smile: .Yes, some chunk of my code can be found on internet. So don't be astonished, if you come up with any file online with same chunk of code :grimacing:

## What is QtileWM ?

Qtile is a free and open source tilling window manager for the X Window System, written and confiqured in Python. It allows users to customize layouts, widgets, and buit-in commands. A full-featured, hackable tiling window manager.

## About

- Terminal ==> [Alacritty](https://alacritty.org/)
- Compositor ==> [Picom](https://github.com/pijulius/picom/)
- Application Launcher ==> [DMenu](https://tools.suckless.org/dmenu/)
- Menu ==> [XMenu](https://github.com/phillbush/xmenu/)
- Notification ==> [Dunst](https://github.com/dunst-project/dunst)

## Support

If you encounter any issue while using my configs, feel free to open an issue or dm me on discord @Prateekshit Jaiswal#4614

## License 

The files and scripts in this repository are licensed under the [GNU GPLv3 License](https://gitlab.com/prateekshitjaiswal73/dotfiles/-/blob/main/LICENSE) .

